﻿module.exports = function (server) {
	routes = require('./routes')(server);

	for (var route in routes) {
		for (var method in routes[route].methods) {
			if (routes[route].methods[method].restrict && routes[route].methods[method].restrict.length > 0) {
				server.app[method](route, routes[route].methods[method].restrict, routes[route].methods[method].callBack);
			}
			else {
				server.app[method](route, routes[route].methods[method].callBack);
			}
		}
	}
};
