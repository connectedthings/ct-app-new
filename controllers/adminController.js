﻿module.exports = function(server){
	//Admin	Controller
	var mdb = require('../mongo')(server.env),
		formModel = require('../models/form'),
		merge = require('merge'),
		deviceConfigModel = require('../models/deviceConfigModel')(server.env),
		dbUtil = require('../mongo/dbCollectionUtil.js');

	var module = {
		Index : function (req, res) {
			mdb.getDocumentByCollectionAndId("deviceConfig", req.session.user.deviceConfigId, function (err, deviceConfig) {
				if (err) {
					res.status(404).send('Not found');
				}

				deviceConfig = deviceConfig ? deviceConfig : [require('../forms/deviceConfig')];
				var formModels = deviceConfig.map(function (item) {
					return formModel.getFormModel(item);
				});

				res.render('Admin/index', {
					authenticated: (req.session.user ? true : false),
					title: 'Admin Connected Things',
					year: new Date().getFullYear(),
					deviceConfig: formModels,
					id: req.session.user.deviceConfigId ? req.session.user.deviceConfigId : ''
				});
			});
		},
		
		IndexPost : function (req, res) {
			res.setHeader('Content-Type', 'application/json');
			mdb.getDocumentByCollectionAndId("deviceConfig", req.body.id, function (err, deviceConfig) {
				if(err) {
					res.send(JSON.stringify({ success: false }));
				}

				deviceConfig = deviceConfig instanceof Array ? deviceConfig[0] : deviceConfig;
				deviceConfig = merge.recursive(true, deviceConfig, dbUtil.getDeviceConfigObject(req.body));
				var deviceConfigModel = require('../models/deviceConfigModel')(server.env).getDeviceConfigModel([deviceConfig])[0];
				var cacheDeviceConfig = server.cache.get("deviceConfig") || [deviceConfigModel];
				for (var i = 0; i < cacheDeviceConfig.length; i++) {
					if (deviceConfigModel.deviceId === cacheDeviceConfig[i].deviceId) {
						cacheDeviceConfig[i] = deviceConfigModel;
						break;
					}
				}
				server.cache.set("deviceConfig",cacheDeviceConfig);
				mdb.setDocumentByCollectionAndObj("deviceConfig", deviceConfig, function (err, result) {
					if (!err) {
						res.send(JSON.stringify({ success: true }));
						}
					else {
						res.send(JSON.stringify({ success: false }));
					}
				});
			});
		}
	};

	return module;
};