﻿module.exports = function (server) {
	var mdb = require('../mongo')(server.env),
		user = require('../models/user'),
		data = require('../datastream')(server);
		deviceConfigModel = require('../models/deviceConfigModel')(server.env);

	var module = {
		
		//Login Form
		LoginGet : function (req, res) {
			if (!req.session.user) {
				res.render('User/login', { 
					authenticated : (req.session.user ? true : false), 
					title: 'Login', 
					year: new Date().getFullYear(),
					message : res.locals.message,
					redirectUrl : req.session.redirectUrl ? req.session.redirectUrl : '/'
				});
			}
			else {
				res.redirect('/');
			}
		},
		
		//Login Post
		LoginPost : function (req, res) {
			mdb.getUser(req.body.username, function (err, result) {
				if (err) {
					console.log(err);
					res.redirect('login');
				}
				else {
					console.dir(result);
					console.log(user);
					user.authenticate(result, req.body.password, function (err, user) {
						console.log(user);
						if (user) {
							// Regenerate session when signing in
							// to prevent fixation
							req.session.regenerate(function () {
								// Store the user's primary key
								// in the session store to be retrieved,
								// or in this case the entire user object
								req.session.user = user;
								req.session.success = 'Authenticated as ' +
												user.name +
												' click to <a href="/logout">logout</a>. ' +
												' You may now access <a href="/restricted">/restricted</a>.';
								console.log("login succeded:" + user.name);
								var redirectUrl = req.body.redirectUrl ? req.body.redirectUrl : '/';
								deviceConfigModel.getDeviceConfig('59f372d4e9ed77180dcfeeba', function (err, deviceConfig) {
									if (!err && deviceConfig) {
										server.cache.flushAll();
										server.cache.set("deviceConfig", deviceConfig);
										server.cache.set("bufferSize", 30);
										server.cache.set("timmerInterval", 2000);
										if (!data.dataStreamingOn() && deviceConfig) {
											data.startStreamForDevices(deviceConfig);
										}
									}
									else {
										res.redirect("/admin/index");
									}
									res.redirect(redirectUrl);
								});
								
							});
						}
						else {
							console.log('test');
							req.session.error = 'test1 Authentication failed, please check your username and password.';
							console.log(req.session.error);
							res.redirect('/login');						
							
						}
					});
				}
			});
		},
		
		//Logout
		Logout : function (req, res) {
			// destroy the user's session to log them out
			// will be re-created next request
			data.stopStreamData(function(){
				server.cache.flushAll();
				req.session.destroy(function () {
					res.redirect('/');
				});
			});
		}
	};
	
	return module;
}