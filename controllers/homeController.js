﻿module.exports = function (server) {
	var data = require('../datastream')(server),
		mdb = require('../mongo')(server.env),
		cache = {},
		deviceConfigModel = require('../models/deviceConfigModel')(server.env);

	return {
		//Home
		Index: function (req, res) {
			var id = req.query.deviceId;
			//id = req.session.user.deviceConfigId;

			var deviceModel = {
				deviceModel: true
			};

			var deviceConfig = server.cache.get("deviceConfig");

			if (!data.dataStreamingOn() && deviceConfig) {
				data.startStreamForDevices(deviceConfig);
				console.log('start streaming');
			}

			deviceModel.devices = deviceConfig;
			deviceModel.currentDevice = id ? deviceModel.devices.indexOf(deviceModel.devices.find(function (item) {
				return item.deviceId === id;
			})) : 0;
			deviceModel.deviceId = id ? id : deviceModel.devices[0].deviceId;

			var model = {
				authenticated: (req.session.user ? true : false),
				title: 'Home Connected Things',
				year: new Date().getFullYear(),
				model: deviceModel
			};

			res.render('Home/index', model);
		},

		Sensor: function (req, res) {
			var id = req.query.deviceId,
				sensorType = req.query.sensor,
				isAjaxRequest = req.xhr;

			if (!id || !sensorType) {
				res.status(404).send('Not found');
			}
			
			var deviceConfig = server.cache.get("deviceConfig");

			if (!data.dataStreamingOn() && deviceConfig) {
				data.startStreamForDevices(deviceConfig);
				console.log('start streaming');
			}

			data.getData(id, true, function (result) {
				var deviceModel = {
					deviceModel: false,
					sensorType: sensorType,
					deviceId: req.query.deviceId,
					sensorData: result
				};

				deviceModel.devices = deviceConfig;
				deviceModel.currentDevice = id ? deviceModel.devices.indexOf(deviceModel.devices.find(function (item) {
					return item.deviceId === id;
				})) : 0;
				deviceModel.sensorConfig = deviceModel.devices[deviceModel.currentDevice].sensors.find(function (item) {
					return sensorType === item.sensorType;
				});
				deviceModel.sensorName = deviceModel.sensorConfig.sensorName;

				deviceModel.sensorValue = result && result.length && result[result.length - 1].with ? result[result.length - 1].with[0].content[sensorType].toFixed(2) : '';
				deviceModel.resolution = server.cache.get("bufferSize");


				res.render(isAjaxRequest ? 'Dashbourd/sensorAjax' : 'Dashbourd/sensor', {
					authenticated: (req.session.user ? true : false),
					title: 'Home Sensors Connected Things',
					year: new Date().getFullYear(),
					model: deviceModel
				});
			});
		},

		Dashbourd: function (req, res) {
			res.render('Dashbourd/dashbourd', {
				authenticated: (req.session.user ? true : false),
				title: 'Home Connected Things',
				year: new Date().getFullYear(),
				model: deviceModel
			});
		},

		SensorThresholds: function (req, res) {
			res.setHeader('Content-Type', 'application/json');
			var deviceId = req.body.deviceId;
			var sensorType = req.body.sensorType;
			if (!deviceId || !sensorType) {
				console.error(new Error("Device Id or sensor type missing "));
				res.send(JSON.stringify({ success: false }));
			}
			mdb.getDocumentsByCollectionAndQueryObj('deviceConfig',{'deviceId.value' : deviceId}, function (err, deviceConfig) {
				if (err || !deviceConfig) {
					console.error(err || new Error("Cannot get deviceCofig Object for device : " + deviceId));
					res.send(JSON.stringify({ success: false }));
				}

				deviceConfig = deviceConfig instanceof Array ? deviceConfig[0] : deviceConfig;

				try {
					var sensor = deviceConfig.sensors[sensorType];
					var value = JSON.parse(sensor.value.replace(/'/g, "\""));
					for (var prop in value) {
						if (req.body.hasOwnProperty(prop)) {
							value[prop] = req.body[prop];
						}
					}
					sensor.value = JSON.stringify(value);
					deviceConfig.sensors[sensorType] = sensor;
				}
				catch (e) {
					console.error(e);
					res.send(JSON.stringify({ success: false }));
				}

				var deviceConfigModel = require('../models/deviceConfigModel')(server.env).getDeviceConfigModel([deviceConfig])[0];
				var cacheDeviceConfig = server.cache.get("deviceConfig") || [deviceConfigModel];
				for (var i = 0; i < cacheDeviceConfig.length; i++) {
					if (deviceConfigModel.deviceId === cacheDeviceConfig[i].deviceId) {
						cacheDeviceConfig[i] = deviceConfigModel;
						break;
					}
				}
				server.cache.set("deviceConfig", cacheDeviceConfig);
				mdb.setDocumentByCollectionAndObj("deviceConfig", deviceConfig, function (err, result) {
					if (!err) {
						res.send(JSON.stringify({ success: true }));
					}
					else {
						res.send(JSON.stringify({ success: false }));
					}
				});
			});
		}
	};
};