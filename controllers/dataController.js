﻿module.exports = function (server) {
	var mdb = require('../mongo')(server.env);
	var data = require('../datastream')(server);

	return {
		enableStreamData : function (req, res) {
			data.startStreamForDevices();
		},
		
		extractData : function (req, res) {
			var getAll = req.query.getAll ? true : false,
				deviceId = req.query.deviceId;
			    sensorType = req.query.sensor;
			res.setHeader('Content-Type', 'application/json');

			if (!deviceId) {
				res.jsonp({ success: false });
			}

			if (!data.dataStreamingOn()) {
				data.startStreamForDevices();
			}
			
			data.getData(deviceId, getAll, function (result) {
				res.jsonp(result || { success: false });
			});
		}
	};
};