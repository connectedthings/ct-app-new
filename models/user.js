﻿var hash = require('../pass').hash,
	guard = require('../guard');

var authenticate = function (users, pass, next) {
	if (!users || users.length == 0) return next(new Error('cannot find user'));
	// apply the same algorithm to the POSTed password, applying
	// the hash against the pass / salt, if there is a match we
	// found the user
	var user = users[0];
	 
	hash(pass, user.salt, function (err, hash) {
		if (err) return next(err);
		if (hash.toString('base64') == user.hash) {
			return next(null, user);
		}
		next(new Error('invalid password'));
	});

};

var requireRole = function (role) {
	return function (req, res, next) {
		guard.authenticated(req, res, function () {
			if (req.session.user && req.session.user.rolls) {
				for (var i = 0; i < req.session.user.rolls.length; i++) {
					if (req.session.user.rolls[i] === role) {
						next();
					}
				}
			}
			else {
				res.send(403);
			}
		});
	}
};

module.exports.authenticate = authenticate;
module.exports.requireRole = requireRole;