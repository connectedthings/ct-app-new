var extend = require('util')._extend;

var getFormModel = function (mongoDbObj,parent) {
    var formModel = [];
    for(var prop in mongoDbObj){ 
        if (prop.indexOf('_') === -1) {
            if (mongoDbObj[prop] && typeof mongoDbObj[prop] === 'object' && mongoDbObj[prop].hasOwnProperty('value')) {
                formModel.push(extend(mongoDbObj[prop], { name: ((parent ? parent : '') + prop) }));
            }
            else if (mongoDbObj[prop] && typeof mongoDbObj[prop] === 'object' && !mongoDbObj[prop].hasOwnProperty('value')) {
                var parentProp = (parent ? parent : '') + prop + '.'; 
                Array.prototype.push.apply(formModel, getFormModel(mongoDbObj[prop], parentProp));
            }
        }
    }

    return formModel;
}

module.exports.getFormModel = getFormModel;