module.exports = function (env) {
    var mdb = typeof env === 'string' ? require('../mongo')(env) : env,
        getDeviceConfigModel = function (deviceConfig) {
            return deviceConfig instanceof Array ? deviceConfig.map(function(obj){
                var device = {
                    deviceName: obj.deviceName.value,
                    deviceId: obj.deviceId.value,
                    protocol: obj.protocol.value,
                    host : obj.host.value,
                    path : "/" + obj.url.value + "/",
                    sensors: []
                };

                for (var prop in obj.sensors) {
                    try {
                        var val = JSON.parse(obj.sensors[prop].value.replace(/'/g, "\""));
                    }
                    catch (e) {
                        console.log(e);
                    }
                    var sensorObj = {
                        sensorType: prop,
                        value: 0,
                        faClass: "fa-dashboard"
                    };
                    for(var key in val) {
                        sensorObj[key] = val[key];
                    }
                    device.sensors.push(sensorObj);
                }

                return device;
            }) : null;
        };

    return {
        getDeviceConfig: function (deviceConfigId, next) {
            mdb.getDocumentByCollectionAndId("deviceConfig", deviceConfigId, function (err, deviceConfig) {
                if (err) {
                    next(err, null);
                }

                next(null, getDeviceConfigModel(deviceConfig));
            });
        },

        "getDeviceConfigModel" : getDeviceConfigModel
    };
};