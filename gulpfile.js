﻿var gulp = require('gulp');
var less = require('gulp-less');
var lessimport = require('gulp-less-import');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var html2jade = require('gulp-html2jade');
var path = require('path');
var options = { nspaces: 2 };

var appPath = './app/';
var destPath = './public/dist';

gulp.task('jade', function () {
	gulp.src('./html/index.html')
    .pipe(html2jade(options))
    .pipe(gulp.dest(destPath));
});

gulp.task('scripts', function(){
    gulp.src([appPath + '/js/app/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest(destPath));
});

gulp.task('less', function () {
  return gulp.src(appPath + '/less/**/*.less')
    .pipe(lessimport('style.less'))
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ],
    }))
    .pipe(gulp.dest(destPath));
});

gulp.task('watch', function() {
    gulp.watch(appPath + '/less/**/*.less', ['less']);  // Watch all the .less files, then run the less task
    gulp.watch(appPath + '/js/app/**/*.js', ['scripts']);  // Watch all the .js files, then run the scripts task
});

gulp.task('default', ['less', 'scripts']);