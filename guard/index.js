﻿(function (guard) {	
	guard.authenticated = function (req, res, next) {
		if (req.session.user) {
			next();
		} else {
			req.session.redirectUrl = req._parsedUrl.path;
			return res.redirect('/login');
		}
	};
})(module.exports)