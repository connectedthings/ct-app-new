﻿module.exports = function (server) {
	if (!server) { return new Error("Server instance parameter missing") }

	var userController = require('../controllers/userController')(server),
		homeController = require('../controllers/homeController')(server),
		dataController = require('../controllers/dataController')(server),
		adminController = require('../controllers/adminController')(server),
		user = require('../models/user'),
		guard = require('../guard');
	
	//Create routes config object
	var	routesConfig = {
			//Home Controller
			'/': {
				methods: {
					'get': {
						restrict: guard.authenticated,
						callBack: homeController.Index
					}
				},
			},

			'/sensor': {
				methods: {
					'get': {
						restrict: guard.authenticated,
						callBack: homeController.Sensor
					}
				},
			},

			'/sensor/thresholds': {
				methods: {
					'post': {
						restrict: user.requireRole("admin"),
						callBack: homeController.SensorThresholds
					}
				},
			},

			'/dashbourd': {
				methods: {
					'get': {
						restrict: guard.authenticated,
						callBack: homeController.Dashbourd
					}
				},
			},

			//User Controller
			'/logout': {
				methods: {
					'get': {
						callBack: userController.Logout
					}
				}
			},

			'/login': {
				methods: {
					'get': {
						callBack: userController.LoginGet
					},
					'post': {
						callBack: userController.LoginPost
					}
				}
			},

			//Data Controller
			'/getsensordata': {
				methods: {
					'get': {
						restrict: guard.authenticated,
						callBack: dataController.extractData
					}
				}
			},

			//Admin Controller
			'/admin/index': {
				methods: {
					'get': {
						restrict: user.requireRole("admin"),
						callBack: adminController.Index
					},
					'post': {
						restrict: user.requireRole("admin"),
						callBack: adminController.IndexPost
					}
				}
			}
		};

	return routesConfig;
};