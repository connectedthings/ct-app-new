﻿module.exports = function (env) {
	if (!env) { return new Error("Environment parameter missing") }

	var mongodb = require('mongodb'),
		dbConfig = require('../appConfig')[env].db,
		mongoUrl = "mongodb://" + dbConfig.user + 
				":" + dbConfig.pass + 
				"@" + ( env == "prod" ? (process.env[dbConfig.dbHost] || "mongodb-26-rhel7") : dbConfig.dbHost) + 
				":" + ( env == "prod" ? (process.env[dbConfig.dbPort] || 27017) : dbConfig.dbPort) +
				"/" + dbConfig.dbName,
		dataBase;

	return {
		getDB : function (next) {
			if (!dataBase) {
				if (mongoUrl) {
					console.log('Mongo URL is %s ...',
							mongoUrl);
					mongodb.MongoClient.connect(mongoUrl, function (err, db) {
						if (!err) {
//							dataBase = {
//								db : db,
//								users : db.collection['users']
//								};
							next(null, db);
						}
						else {
							next(err, null);
						}
						db.close();
					});
				}
				else {
					next(new Error("db Config not initialized"), null);
				}
			}
			else {
				next(null, dataBase);
			}
		}
	};
};

//(function (database) {
//	database = function (env) {

//		if (!env) { return new Error("Environment parameter missing") }

//		var module = {},
//			mongodb = require('mongodb'),
//			dbConfig = require('../appConfig')[env].db,
//			mongoUrl = "mongodb://" + dbConfig.user + 
//				":" + dbConfig.pass + 
//				"@" + dbConfig.dbHost + 
//				":" + dbConfig.dbPort +
//				"/" + dbConfig.dbName,
//			dataBase;

		
//		module.getDB = function (next) {
//			if (!dataBase) {
//				if (mongoUrl) {
//					mongodb.MongoClient.connect(mongoUrl, function (err, db) {
//						if (!err) {
//							dataBase = {
//								db : db,
//								users : db.collection("users")
//							};
//							next(null, dataBase);
//						}
//						else {
//							next(err, null);
//						}
//					});
//				}
//				else {
//					next(new Error("db Config not initialized"), null);
//				}
//			}
//			else {
//				next(null, dataBase);
//			}
//		};
		
//		return module;
//	};
//})(module.exports)