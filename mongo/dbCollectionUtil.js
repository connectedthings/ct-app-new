﻿(function (util) {

	util.getDeviceConfigObject = function (object) {
		var responseObj = {};
		for (var key in object) {
			if(key !== 'id' && object[key]) {
				var childs = key.split('.');
				responseObj[childs[0]] = responseObj[childs[0]] ? responseObj[childs[0]] : {};
				var obj = responseObj[childs[0]];
				for(var i=1; i<childs.length;i++) {
					obj[childs[i]] = obj[childs[i]] ? obj[childs[i]] : {};
					obj = obj[childs[i]];
				}

				obj.value = object[key];
			}
		}

		return responseObj;
	};
	 
})(module.exports)