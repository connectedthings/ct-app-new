﻿module.exports = function (env) {
	if (!env) { return new Error("Environment parameter missing") }
	var ObjectId = require('mongodb').ObjectID;
	var database = require('./database')(env);
	
	return {
		getUsers : function (next) {
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					db.collection("users").find().toArray(function (err, result) {
						if (err) {
							console.log(err);
						}
						else {
							next(null, result);
						}
					});
				}
			});
		},
		
		getUser : function (userEmail, next) {
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					db.collection("users").find({"email": userEmail}).toArray(function (err, result) {
						if (err) {
							console.log(err);
						}
						else {
							next(null, result);
						}
					});
				}
			});
		},
		
		getDocumentByCollectionAndId : function (collectionName, id, next) {
			if (!collectionName || !id) {
				next(null, null);
			}
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					try {
						var ids = [];
						if (id instanceof Array) {
							for (var i=0; i<id.length;i++) {
								ids.push(ObjectId(id[i]));
							}
						}
						db.collection(collectionName).find().toArray(function (err, result) {
							if (err) {
								console.log(err);
								next(err, null);
							}
							else {
								next(null,result);
							}
						});
					}
					catch (e) {
						console.log(e);
						next(e, null);
					}
				}
			});
		},

		getDocumentsByCollectionAndQueryObj : function (collectionName, query, next) {
			if (!collectionName || !query) {
				next(null, null);
			}
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					try {
						db.collection(collectionName).find(query).toArray(function (err, result) {
							if (err) {
								console.log(err);
								next(err, null);
							}
							else {
								next(null,result);
							}
						});
					}
					catch (e) {
						console.log(e);
						next(e, null);
					}
				}
			});
		},

		setDocumentByCollectionAndObj : function (collectionName, object, next) {
			if (!collectionName) {
				next(new Error("Collection Name not provided"), null);
			}
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					try {
						db.collection(collectionName).save(object, {w:1}, function (err, result) {
							if (err) {
								next(err, null);
							}
							next(null,result);
						});
					}
					catch (e) {
						console.log(e);
						next(e, null);
					}
				}
			});
		},

		setDocumentByCollectionAndId : function (collectionName, id, object, next) {
			if (!collectionName) {
				next(new Error("Collection Name not provided"), null);
			}
			database.getDB(function (err, db) {
				if (err) {
					next(err, null);
				}
				else {
					try {
						db.collection(collectionName).save(object, {w:1}, function (err, result) {
							if (err) {
								next(err, null);
							}
							next(null,result);
						});
					}
					catch (e) {
						console.log(e);
						next(e, null);
					}
				}
			});
		}
	};
};