#!/bin/env node
//  OpenShift Node application
var express = require('express'),
	fs = require('fs'),
	hash = require('./pass').hash,
	env = "prod",
	config = require('./appConfig')[env],
    NodeCache = require('node-cache');
	path = require('path');

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}


/**
 *  Define the sample application.
 */
var CtApp = function() {

    //  Scope.
    this.cache = new NodeCache();
    this.env = env;
    var self = this;


    /*  ================================================================  */
    /*  Helper functions.                                                 */
    /*  ================================================================  */

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
		//  Set the environment variables we need.
		self.ipaddress = env === 'prod' ? (process.env[config.system.IP] || "0.0.0.0") : config.system.IP;
		self.port = env === 'prod' ? (process.env[config.system.PORT] || 8080) : (config.system.PORT || 3000);

        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };


    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
    self.terminator = function(sig){
        if (typeof sig === "string") {
           console.log('%s: Received %s - terminating sample app ...',
                       Date(Date.now()), sig);
           process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()) );
    };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
    self.setupTerminationHandlers = function(){
        //  Process on exit and signals.
        process.on('exit', function() { self.terminator(); });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
         'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
            process.on(element, function() { self.terminator(element); });
        });
    };


    /*  ================================================================  */
    /*  App server functions (main app logic here).                       */
    /*  ================================================================  */


    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function() {
		self.app = express();
		self.app.set('views', path.join(__dirname, 'views'));
		self.app.set('view engine', 'jade');
		self.app.use(express.static(path.join(__dirname, 'public')));
		self.app.use(express.bodyParser());                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		self.app.use(express.cookieParser(config.web.cookieParser));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
		self.app.use(express.session());
		self.app.use(express.favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
		self.app.use(express.logger('dev'));
		self.app.use(express.methodOverride());

		self.app.use(function(req, res, next){ 
			var err = req.session.error
			    ,msg = req.session.success;                                                                                                                                                                                                                                                       
			delete req.session.error;                                                                                                                                                                                                                                                 
			delete req.session.success;                                                                                                                                                                                                                                                      
			res.locals.message = '';                                                                                                                                                                                                                                                      
			if (err) res.locals.message =  err;                                                                                                                                                                                                                    
			if (msg) res.locals.message =  msg;                                                                                                                                                                                                                  
			next();                                                                                                                                                                                                                                                      
		});

		//init routes
		require('./initRoutes')(self);
    };


    /**
     *  Initializes the sample application.
     */
    self.initialize = function() {
        self.setupVariables();
        self.setupTerminationHandlers();

        // Create the express server and routes.
        self.initializeServer();
    };


    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        //  Start the app on the specific interface (and port).
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...',
                        Date(Date.now() ), self.ipaddress, self.port);
        });
    };

};   /*  Sample Application.  */



/**
 *  main():  Main code.
 */
var zapp = new CtApp();
zapp.initialize();
zapp.start();

