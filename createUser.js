﻿var hash = require("./account").hash,
	fs = require('fs'),
	prompt = require('prompt');

prompt.start();

prompt.get(['username', 'email','password','rolls'], function (err, result) {
	if (err) { return onErr(err); }
	hash(result.password, function (error, salt, hashpass) {
		if (error) {
			return onErr(error);
		}
		var obj = {
			"email": result.email,
			"name": result.username,
			"hash": hashpass.toString('base64'),
			"salt": salt,
			"rolls": result.rolls.split(',')
		};
		var users = [];
		users.push(obj);
		
		fs.writeFileSync('./userdata.json', JSON.stringify(users));
	});
});


function onErr(err) {
	console.log(err);
	return 1;
}