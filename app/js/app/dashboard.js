var app = (function(app){
	'use strict';

	var app = app || {},
	nReloads = 0,
	NS = 'dashboard',
	deviceId = '',
	refreshThreshold = function(){
		$('.panel[data-min][data-max]').each(function(){
			$(this).removeClass().addClass("panel panel-" + ($(this).data('value') >= $(this).data('min') && $(this).data('value') <= $(this).data('max') ? 'green' : 'red'));
		});
	},
	pool = function(options) {
		$.ajax({
			url:"/getsensordata?" + 'deviceId=' + deviceId,
			dataType: 'jsonp',
			success : function(data){
				if(data && (data["this"] === "succeeded")) {
					for (var key in data.with[0].content){
						var displayValue = data.with && data.with.length > 0 ? data.with[0].content[key].toFixed(2) : 0;
						$('#'+key+' .huge').html(displayValue);
						$('#'+key).data('value',displayValue);
					}
					refreshThreshold();
				}

				setTimeout(pool, 2000);
			},
			error : function(error){
				console.log("Error", error);
			}      
		});		
	};

	app[NS] = function(options) {
		deviceId= options.deviceId
		pool();
	};

	return app;
})(app);