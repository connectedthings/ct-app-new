var app = (function(app){
    'use strict';

    var app = app || {},
        NS = 'dataProvider',
        DataProvider = function(options) {
            var req = $.ajax({
                url:"/getsensordata?deviceId=" + options.deviceId,
                dataType: 'jsonp'
            });

            return req;
        };

    app[NS] = function(options) {
        return DataProvider(options);    
    };

    return app;
})(app);