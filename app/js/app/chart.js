var app = (function(app, Morris){
    'use strict';

    var app = app || {},
        Morris = Morris || undefined,
        nReloads = 0,
        NS = 'chart',
        defaults = {
            resolution: 20
        },
        chartConfig = {
            'temp': {
                color: 'red'
            },
            'vibrate': {
                color: 'orange'
            },
            'magnet': {
                color: 'green'
            },
            'accel': {
                color: 'violet'
            },
            'humid': {
                color: 'blue'
            },
            'acoustic': {
                color: 'yellow'
            }
        },
        getThreshhold = function(options) {
            var model = options.model;

            return model.devices[model.currentDevice].sensors.find(
                function(elem){ 
                    return elem.sensorType === options.type 
                }
        )},

        mapData = function(data) { 
            data = Array.isArray(data) ? data : [data];

            var mappedData = data.map(function(current){
                return {
                    time: current.with[0].created,
                    value: current.with[0].content[this.type]
                }
            }.bind(this));

            return mappedData;
        },
        
        // Constructor
        Chart = function(options) {
            var self = this,
                model = getThreshhold(options);

            this.minValue = model.min;
            this.maxValue = model.max;

            this.type = options.type;
            this.data = mapData.call(this, options.data);
            this.resolution = options.resolution || defaults.resolution;
            this.graphColor = chartConfig[this.type].color;
            
            this.graph = Morris.Line({
                element: 'graph-box',
                data: this.data,
                xkey: 'time',
                ykeys: ['value'],
                labels: ['Value', 'Time'],
                xLabels: 'second',
                lineColors: [this.graphColor],
                goals: [this.minValue, this.maxValue],
                goalLineColors: ["red", "red"],
                dateFormat: function(date) {
                    return new Date(date).toTimeString();
                },
                parseTime: true,
                hideHover: true,
                resize: true
            });

            if (this.graph) {
                function pool(){
                    self.chartDataProvider = app.dataProvider({
                        type: self.type,
                        deviceId : options.deviceId
                    });

                    self.chartDataProvider.then(function(data){
                        var newData = mapData.call(self, data),
                            value = newData[0].value;

                        if (self.data.length > self.resolution) {
                            self.data.shift();
                        }

                        self.data.push(newData[0]);
                        self.update(self.data);

                        self.renderHeader(value);

                        setTimeout(pool, 1000);
                    });
                };

                setTimeout(pool, 2000);
            }
            
            return Promise.resolve(this.graph);
        };

    Chart.prototype.renderHeader = function(value) {
        // Update sensor value
        var elSensor = $('.sensor-value');
        
        elSensor.addClass('alert-success');
        elSensor.html(value.toFixed(2));

        if (value < this.minValue || this.maxValue < value) {
            elSensor.addClass('alert-danger');
        } else {
            elSensor.removeClass('alert-danger');
        }
    };

    Chart.prototype.update = function(data) {
        this.graph.setData(data);
    }

    app[NS] = function(options) {
        if (Morris) {
            return new Chart(options);
        } else {
            console.error('Chart plugin is not available');
        }
        
    }

    return app;
})(app, Morris);