﻿module.exports = function (mqttServerConfig) {
	var options = mqttServerConfig ? {
		protocolId: mqttServerConfig.protocolId, 
		protocolVersion : mqttServerConfig.protocolVersion,
		connectTimeout: mqttServerConfig.connectTimeout,
		debug: mqttServerConfig.debug,
		clientId : mqttServerConfig.clientId
	} : undefined,
		mqtt = require("mqtt");
	return mqtt.connect(mqttServerConfig.address, options)
}


//client.on('connect', function () {
//	//subscribe to configuration topics
//	for (var i = 0; i < mqttServerConfig.topics.length; i++) {
//		client.subscribe(mqttServerConfig.topics[i]);
//	}
//});

//var count = 0;

//client.on('message', function (topic, message) {
//	// message is Buffer
//	console.log(topic + " : " + message.toString());
//	//client.end();
//});
//client.on('close', function () {
//	console.log("Close");
//});

//client.on('error', function () {
//	console.log("ERROR");
//	client.end();
//});