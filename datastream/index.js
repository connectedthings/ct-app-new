﻿module.exports = function (server) {
	server.cache.set("bufferSize", 30);
	server.cache.set("timmerInterval", 2000);
	async = require('async'),
		httpGet = function (deviceConfig, callback) {
			if (deviceConfig.deviceId === "De4D-CbDoe_5") {
				//callback(null, true);
				var client = require(deviceConfig.protocol === 'https' ? 'https' : 'http');
				var options = {
					hostname: deviceConfig.host,
					port: deviceConfig.protocol === 'https' ? 443 : 80,
					path: deviceConfig.path + '/' + deviceConfig.deviceId,
					method: 'GET'
				};
				var body = '';
				var req = client.request(options, function(res) {
					res.on('data', function(d) {
						var result = JSON.parse(d);
						var extractedData = server.cache.get(deviceConfig.deviceId) || require('fifo')();
						if (extractedData.length > server.cache.get("bufferSize")) {
							extractedData.shift();
						}
						if (result && result["this"] === "succeeded") {
							result.with[0].created = Date.now();
							extractedData.push(result);
						}
						server.cache.set(deviceConfig.deviceId, extractedData);
						callback(null, true);
					});
				});
				req.end();

				req.on('error', function(e) {
					console.error(e);
					callback(e);
				});
			}
			else {
				callback(null, true);
			}
		},
		getSensorData = function (devices, next) {
			async.map(devices, httpGet, function (err, result) {
				if (err) { 
					console.log(err);
					next(err);
				}
				else {
					next(null);
				}
			});
		},
		getMockSensorData = function (deviceId, callback) {
			//console.log("ttttttttttt"+deviceId);
			callback(null, {
				"this": "succeeded",
				"by": "getting",
				"the": "dweets",
				"with": [
					{
						"thing": deviceId,
						"created": Date.now(),
						"content": {
							"acoustic": Number(Math.floor((Math.random() * 1000) + 100).toString() + '.' + Math.floor((Math.random() * 100) + 10).toString()),
							"magnet": Number(Math.random().toFixed(6)),
							"temp": Number(Math.floor((Math.random() * 100) + 10).toString() + '.' + Math.floor((Math.random() * 100) + 10).toString()),
							"humid": Number(Math.floor((Math.random() * 100) + 10).toString() + '.' + Math.floor((Math.random() * 100) + 10).toString()),
							"accel": Number(Math.floor((Math.random() * 10) + 1).toString() + '.' + Math.floor((Math.random() * 100) + 10).toString()),
							"vibrate": Number(Math.floor((Math.random() * 10) + 1).toString() + '.' + Math.floor((Math.random() * 100) + 10).toString())
						}
					}
				]
			});
		};

	//module exposed methods
	return {
		dataStreamingOn: function (deviceId) {
			var dataStreaming = server.cache.get('dataStreaming') && server.cache.get('timer');
			return !!dataStreaming;
		},

		getData: function (deviceId, getAll, next) {
			var extractedData = server.cache.get(deviceId);
			if (2 === arguments.length) {
				next = getAll;
			}
			if (getAll && !extractedData && 3 === arguments.length) {
				getSensorData(server.cache.get("deviceConfig"),function(err){
					extractedData = server.cache.get(deviceId);
					var dataValues = [];
					if (!err && extractedData && extractedData.length > 0) {
						var dataValues = [];
						for (var node = extractedData.node; node; node = extractedData.next(node)) {
							dataValues.push(node.value);
						}
					}
					next(dataValues)
				});
			}
			else {
				if (3 === arguments.length && extractedData && extractedData.length > 0 && getAll) {
					var dataValues = [];
					for (var node = extractedData.node; node; node = extractedData.next(node)) {
						dataValues.push(node.value);
					}
					next(dataValues);
				}
				else {
					next(extractedData && extractedData.length > 0 ? extractedData.last() : null);
				}
			}
		},

		startStreamForDevices: function (devices) {

			var pool = function() {
				getMockSensorData(devices,function(err){
					if(!err) {
						server.cache.set('timer', setTimeout(pool, server.cache.get("timmerInterval")));
						server.cache.set('dataStreaming',true);
					}
					else {
						server.cache.del('dataStreaming');
					}
				});
			};

			server.cache.set('timer', setTimeout(pool, server.cache.get("timmerInterval")));
			server.cache.set('dataStreaming',true);
		},

		stopStreamData : function (next) {
			var timer = server.cache.get('timer');
			clearInterval(timer);
			server.cache.set('timer',0);
			server.cache.del('timer');
			next();
		}
	};
};
